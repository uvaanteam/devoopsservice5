var incomingIP = context.getVariable("proxy.client.ip");
    var whiteListedIPs = context.getVariable("whiteListed_IPs");
    
    var whiteListedIPsArray;
    
    if(whiteListedIPs)
    {
        whiteListedIPs = whiteListedIPs.replace(/\s/g,"");
        whiteListedIPsArray = whiteListedIPs.split(",");
            if(whiteListedIPsArray.indexOf(incomingIP) === -1)
            {
                context.setVariable("errorJSON","invalid_ip");
                throw "invalidip"
            }
    }

